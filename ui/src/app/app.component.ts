import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

import { Http, RequestOptions,Headers,Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private http: Http){

  }
  title = 'app';
  card: Card = new Card();
  expDate: string = "";
  users: Array<any> = new Array<any>();
  message = "";
  user: User = new User();

  ngOnInit(){
    // this.users = [{
    //   userId: 123,
    //   userName: "Subrat",
    //   cards: [{
    //     card: "hfkdshfkjfh",
    //     cvv: "123",
    //     name: "test",
    //     expiry: new Date(),
    //     cardLimit: 120000,
    //     spent: 2000
    //   },
    //   {
    //     card: "hfkdshfkjfhds",
    //     cvv: "123",
    //     name: "test",
    //     expiry: new Date(),
    //     cardLimit: 120000,
    //     spent: 2000
    //   },
    //   {
    //     card: "hfkdshfkjfhgf",
    //     cvv: "123",
    //     name: "test",
    //     expiry: new Date(),
    //     cardLimit: 120000,
    //     spent: 2000
    //   }]
    // },
    // {
    //   userId: 125,
    //   userName: "Subrat",
    //   cards: [{
    //     card: "hfkdshfkjfh",
    //     cvv: "123",
    //     name: "test",
    //     expiry: new Date(),
    //     cardLimit: 120000,
    //     spent: 2000
    //   },
    //   {
    //     card: "hfkdshfkjfhds",
    //     cvv: "123",
    //     name: "test",
    //     expiry: new Date(),
    //     cardLimit: 120000,
    //     spent: 2000
    //   },
    //   {
    //     card: "hfkdshfkjfhgf",
    //     cvv: "123",
    //     name: "test",
    //     expiry: new Date(),
    //     cardLimit: 120000,
    //     spent: 2000
    //   }]
    // },
    // {
    //   userId: 125,
    //   userName: "Subrat",
    //   cards: []
    // }]

    const url = "http://localhost:9090/user/users"
    this.http.get(url).map(res => res.json()).subscribe(
      data => {
        this.users = data;
      },err => {
        this.message = "error";
      }
    )
  }

  checkCard(card){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let option = new RequestOptions({ headers: headers });
    let url = "http://localhost:9090/cps/checkCard";
    this.http.post(url, card, option).map((res: Response) => res.text()).subscribe(
      data => {
        if(data == "success"){
          let url1 = "http://localhost:9090/cps/checkValue";
          this.http.post(url, card, option).map((res: Response) => res.text()).subscribe(
            data => {
              if(data == "success"){
                alert("Success");
              }else{
                alert("fail");
              }
            }
          )
        }
      }
    );
  }

  deleteCard(card, userId){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let option = new RequestOptions({ headers: headers });
    let url = "http://localhost:9090/user/deleteCard/"+userId+"/"+card.card;
    this.http.delete(url).map(res => res.text()).subscribe(
      data => {
        if(data == "success"){
          this.users = this.users.map(u => u.cards.splice(card, 1));
          this.ngOnInit();
        }else{
          this.message = "Some technical error happened."
        }
      }
    )

  }

  addCard(userId){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let option = new RequestOptions({ headers: headers });
    let url = "http://localhost:9090/user/addCard/"+userId;
    this.http.post(url,this.card,option).map(res => res.json()).subscribe(
      data => {
        this.ngOnInit();
      },
      err => {
        this.message = "error";
        console.log(err);
      });
  }

  addUser(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let option = new RequestOptions({ headers: headers });
    console.log(JSON.stringify(this.user,null,2));
    let url = "http://localhost:9090/user/addUser";
    this.http.post(url,this.user,option).map(res => res.text()).subscribe(
      data => {
        this.ngOnInit();
      },
      err => {
        this.message = "error";
        console.log(err);
      });
  }
}

export class Card{
  name: string = "";
  card: string = "";
  expiry : Date;
  cvv: number;
  amount: number;
  cardLimit: number;
  spent: number;
}


export class User{
  userId: number;
  userName: String;
  cards: Array<Card>;
}