package com.cps.services;

import com.cps.cont.CpsCont;
import com.cps.entity.Card;
import com.cps.reop.CardRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardServices {

//	private static final Logger log = LoggerFactory.getLogger(CardServices.class);
	private Logger log = LoggerFactory.getLogger(CardServices.class);
	
    @Autowired
    private CardRepo cardRepo;

    @Autowired
    private BeanFactory beanFactory;

    public String checkCard(Card card){
        String result = "success";
        log.info("inside service method of check card:: ");
        try {
            Card c = cardRepo.findOne(card.getCard());
            if(c == null){
                result = "invalidCard";
                log.debug("invalid card found::: ");
            }else if(!c.getName().equals(card.getName()) || !c.getCvv().equals(card.getCvv()) || !c.getExpiry().getTime().equals(card.getExpiry().getTime())){
                    result = "invalidCard";
                    log.debug("invalid card found::: ");
                }
            
        }catch (Exception e){
            //e.printStackTrace();
            result = "fail";
            log.error("failed in checking card details:: ");
        }
        return result;
    }

    public String checkLimit(Card card){
        String result = "success";
        try{
            CardServices c = beanFactory.getBean(CardServices.class);
            Card cd = cardRepo.findOne(card.getCard());
            if(c.checkCard(card).equals("success")){
                if(card.getAmount() > cd.getCardLimit() && card.getAmount() > (cd.getCardLimit()-card.getSpent())){
                    result = "limitExcide";
                    log.debug(" card limit exceeded found::: ");
                }else{
                    cd.setSpent(cd.getSpent()+card.getAmount());
                    cardRepo.saveAndFlush(cd);
                    log.debug(" card spent amount updated::: ");
                }
            }
        }catch (Exception e){
          //  e.printStackTrace();
            result = "fail";
            log.error("failed in checking card limit:: ");
        }
        return result;
    }

}
