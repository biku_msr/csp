package com.cps.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cps.entity.Card;
import com.cps.entity.User;
import com.cps.reop.CardRepo;
import com.cps.reop.UserRepo;

@Service
public class UserServices {
	
	//private static final Logger log = LoggerFactory.getLogger(UserServices.class);
	private Logger log = LoggerFactory.getLogger(CardServices.class);
	
	@Autowired
    private UserRepo userRepo;
	
	@Autowired
    private CardRepo cardRepo;

	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		List<User> userList=new ArrayList<User>();
		log.info("inside get all user method:: ");
		try{
			userList=userRepo.findAll();	
			
			if(userList!=null){
				log.info("user details found! :: ");
			}
			else{
				log.debug("NO USERS FOUND:: ");
			}
		}catch(Exception e){
			//e.printStackTrace();
			log.error("failed to fetch user details:: ");
		}
		
		
		return userList;
		
	}

	public User getUser(Integer userId) {
		// TODO Auto-generated method stub
		User user=new User();
		log.info("inside get user method:: ");
		try{			
			if(userId!=null){
				user=userRepo.findOne(userId);
				if(user!=null){
					log.info("user found:: ");
				}else
					log.info("user not found:: ");
			}
			
		}catch(Exception e){
			//e.printStackTrace();
			log.error("failed to fetch user details:: ");
		}
		return user;
	}

	public User addCard(Integer userId, Card card) {
		// TODO Auto-generated method stub
		User user=new User();
		Card newCard=new Card();
		log.info("inside add card method:: ");
		try{			
			if(userId!=null && card.getCard()!=null){
				user=userRepo.findOne(userId);
				newCard=cardRepo.findOne(card.getCard());
				if(user!=null && newCard==null){ //if user exists and card does not exist for same or any other user
					user.getCards().add(card);
					user=userRepo.saveAndFlush(user);
					log.info("card added successfully:: ");
				}
				else
					log.info("card cannot be added for user:: "+userId);
			}
		}catch(Exception e){
			//e.printStackTrace();
			log.error("error in adding card for user:: "+userId);
		}
		return user;
	}

	public User deleteCard(Integer userId, String cardNumber) {
		// TODO Auto-generated method stub
		User user=new User();
		Card newCard=new Card();
		log.info("inside delete card method:: ");
		try{			
			if(userId!=null && cardNumber!=null){
				user=userRepo.findOne(userId);
				newCard=cardRepo.findOne(cardNumber);
				if(user!=null && user.getCards().contains(newCard)){ //if user exists and card also exists for same user
					user.getCards().remove(newCard);
					user=userRepo.saveAndFlush(user);
					log.info("card deleted successfully:: ");
				}
				else
					log.info("card cannot be deleted as card not found for user:: ");
			}
		}catch(Exception e){
			//e.printStackTrace();
			log.error("error in deleting card:: "+cardNumber);
		}
		return user;
	}

	public User addUser(User user) {
		// TODO Auto-generated method stub
		User newUser=new User();
		//String msg="";
		try{
			newUser=userRepo.saveAndFlush(user);
			
			if(newUser!=null){
				log.info("user added successfully:: ");
				//msg="success";
			}
			else{
				log.info("user cant be added:: ");
				//msg="fail";
			}
		}catch(Exception e){
			//e.printStackTrace();
			log.error("error in adding new user:: ");
		}
		return newUser;
	}

}
