package com.cps.reop;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cps.entity.User;

public interface UserRepo extends JpaRepository<User,Integer>{

}
