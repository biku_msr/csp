package com.cps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication(scanBasePackages="com.cps")
@EnableJpaRepositories
//@SpringBootApplication
public class CpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CpsApplication.class, args);
	}
}
