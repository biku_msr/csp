package com.cps.cont;

import com.cps.entity.Card;
import com.cps.services.CardServices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cps")
public class CpsCont {

	private Logger log = LoggerFactory.getLogger(CpsCont.class);
	
    @Autowired
    private CardServices cdService;

    @PostMapping("/checkCard")
    public ResponseEntity<String> checkCard(@RequestBody Card request){
    	log.info("inside get check card method:: ");
    	try{
            return new ResponseEntity<String>(cdService.checkCard(request), HttpStatus.OK);
    	}
    	catch(Exception e){
    		log.error("error in checking card method:: ");
    		return null;
    	}
    }

    @PostMapping("/checkValue")
    public ResponseEntity<String> checkValue(@RequestBody Card request){
       	log.info("inside get check value method:: ");
       	try{
       	 return new ResponseEntity<String>(cdService.checkLimit(request), HttpStatus.OK);
       	}
       	catch(Exception e){
    		log.error("error in check value method:: ");
    		return null;
    	}
       
    }

   /* @RequestMapping("/cps")
    public String name(){
        return "Hello";
    }
*/
}
