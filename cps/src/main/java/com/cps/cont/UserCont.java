package com.cps.cont;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cps.entity.Card;
import com.cps.entity.User;
import com.cps.services.UserServices;

@RestController
@RequestMapping("/user")
public class UserCont {
	
	private Logger log = LoggerFactory.getLogger(UserCont.class);
	//private static final Logger log = LoggerFactory.getLogger(UserCont.class);
	
	@Autowired
    private UserServices userService;
	
	
	@RequestMapping("/users")
    public List<User> getAllUser(){
        List <User> userList=new ArrayList<User>();
        try{
        	log.info("inside get all user method");
        	userList=userService.getAllUser();
        	
        	if(userList!=null){
        		log.info("all users fetched successfully :: ");
        	}else{
        		log.debug("users cant be fetched :: ");
        	}
        	
        }
        catch(Exception e){
        	log.error("error in fetching users:: ");
        	//e.printStackTrace();
        }
        
        return userList;
    }
	
	@RequestMapping("/{userId}")
	public User getOneUser(@PathVariable("userId") Integer userId){
		User user=new User();
		
		try{
			log.info("inside get one user method:: ");
			user=userService.getUser(userId);
			
			if(user!=null){
        		log.info("user fetched successfully :: ");
        	}else{
        		log.debug("user cant be fetched :: ");
        	}
			
		}catch(Exception e){
			log.error("error in fetching requested user:: ");
        	//e.printStackTrace();
		}
		return user;
	}
	
	@PostMapping("/addUser")
	public String addUser(@RequestBody User user){
		User newUser=new User();
		String msg="";
		try{
			log.info("inside get one user method:: ");
			newUser=userService.addUser(user);
			
			if(newUser!=null){
        		log.info("user added successfully :: ");
        		msg="success";
        	}else{
        		log.debug("user cant be added :: ");
        		msg="fail";
        	}
			
		}catch(Exception e){
			log.error("error in adding user:: ");
			msg="fail";
        	//e.printStackTrace();
		}
		return msg;
	}
	
	@PostMapping("/addCard/{userId}")
	public User addCard(@PathVariable("userId") Integer userId,@RequestBody Card card){
		User user=new User();
		
		try{
			user=userService.addCard(userId, card);
			if(user!=null){
				log.info("card added for user:: ");
			}else{
				log.debug("card could not be added for user:: ");
			}
				
		}catch(Exception e){
			log.error("error in adding new card for user:: ");
        	//e.printStackTrace();
		}
		
		return user;
	}
	
	@DeleteMapping("/deleteCard/{userId}/{cardNumber}")
	public String deleteCard(@PathVariable("userId") Integer userId, @PathVariable("cardNumber") String cardNumber){
		String msg="";
		User user=new User();
		try{
			user=userService.deleteCard(userId, cardNumber);
			if(user!=null){
				msg="success";
			}else
				msg="fail";
		}catch(Exception e){
			//e.printStackTrace();
		}
		
		return msg;
		
	}
	
}
